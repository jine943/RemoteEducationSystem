// InputIpDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ImageClient.h"
#include "InputIpDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// InputIpDlg dialog


InputIpDlg::InputIpDlg(CWnd* pParent /*=NULL*/)
	: CDialog(InputIpDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(InputIpDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void InputIpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(InputIpDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(InputIpDlg, CDialog)
	//{{AFX_MSG_MAP(InputIpDlg)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// InputIpDlg message handlers

void InputIpDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	
}

BOOL InputIpDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	
//	SetDlgItemText(IDC_IP_EDIT, "58.233.158.53");
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void InputIpDlg::OnOK() 
{
	CString str;
	char str_len = GetDlgItemText(IDC_IP_EDIT, str);
	if(10 < str_len){
		memcpy(m_ip, str, MAX_IP_LEN);
		CDialog::OnOK();
	}
}

char *InputIpDlg::GetIP()
{
	return m_ip;
}
