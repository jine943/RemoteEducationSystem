// ImageClient.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "ImageClient.h"
#include "ImageClientDlg.h"
#include "ImageServerDlg.h"
#include "SelectRemoteModeDlg.h"
#include "InputIpDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImageClientApp

BEGIN_MESSAGE_MAP(CImageClientApp, CWinApp)
	//{{AFX_MSG_MAP(CImageClientApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImageClientApp construction

CImageClientApp::CImageClientApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CImageClientApp object

CImageClientApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CImageClientApp initialization

BOOL CImageClientApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
	WSADATA wsadata;
	WSAStartup(0x0202, &wsadata);

	SelectRemoteModeDlg ins_dlg;
	if(IDOK == ins_dlg.DoModal()){
		g_mode = ins_dlg.GetRemoteMode();

		CImageClientDlg client_dlg;
		m_pMainWnd = &client_dlg;
		client_dlg.DoModal();		
	}
	
	WSACleanup();
	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
