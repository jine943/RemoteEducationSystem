// GuestIpDlg.cpp : implementation file
//

#include "stdafx.h"
#include "imageclient.h"
#include "GuestIpDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GuestIpDlg dialog


GuestIpDlg::GuestIpDlg(CWnd* pParent /*=NULL*/)
	: CDialog(GuestIpDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GuestIpDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	memset(m_ip, 0, MAX_IP_LEN * sizeof(char) * MAX_IP_COUNT);
	memset(m_selected_ip, 0, MAX_IP_LEN * sizeof(char));

}


void GuestIpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GuestIpDlg)
	DDX_Control(pDX, IDC_IP_LIST, m_ip_list);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GuestIpDlg, CDialog)
	//{{AFX_MSG_MAP(GuestIpDlg)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GuestIpDlg message handlers

void GuestIpDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	
}


int TWAPI_GetLocalNetworkAddress(char parm_buffer[][24], int parm_max_count)
{
	int count = 0;
	WSADATA wsadata;

	if(!WSAStartup(DESIRED_WINSOCK_VERSION, &wsadata)){
		if(wsadata.wVersion >= MINIMUM_WINSOCK_VERSION){
			IN_ADDR in_address;
			char host_name[128] = {0, };

			gethostname(host_name, 128);
			
			HOSTENT *p_host_info = gethostbyname(host_name);
			if(NULL != p_host_info){
				for(int i = 0; p_host_info->h_addr_list[i]; i++){
					if(parm_max_count > i){
						memcpy(&in_address, p_host_info->h_addr_list[i], 4);
						strcpy(parm_buffer[i], inet_ntoa(in_address));
					} else break;
				}
				count = i;
			}
		}
		WSACleanup();
	}
	return count;
}


BOOL GuestIpDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	char ip_count = TWAPI_GetLocalNetworkAddress(m_ip, 5);

	for(int i = 0; i < ip_count; i++){
		m_ip_list.InsertString(-1, *(m_ip + i));
	}
	//SetDlgItemText(IDC_IP_EDIT, "58.233.158.53");

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void GuestIpDlg::OnOK() 
{
	CString str;
	int index = m_ip_list.GetCurSel();

	if(LB_ERR != index){
		m_ip_list.GetText(index, str);
		strcpy(m_selected_ip, str);
		CDialog::OnOK();

	} else {
		MessageBox("올바른 IP 를 선택해주세요");
	}
}


char *GuestIpDlg::GetServerIp()
{
	return m_selected_ip;
}
