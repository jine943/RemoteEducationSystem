#ifndef _MY_SOCKET_
#define _MY_SOCKET_

#define LM_DATA_SOCKET_MESSAGE     27000

class MySocket : public CDialog
{
protected:
	SOCKET mh_socket;

	CListBox *mp_event_list;

public:
	MySocket(int parm_dlg_id, CWnd* pParent = NULL);

	virtual void ReadSocketData(SOCKET parm_socket, unsigned short parm_size, unsigned char parm_index, char *parm_body_data);

	void DestroySocket(SOCKET parm_socket);
	char *ReadBodyData(SOCKET parm_h_socket, int parm_size);
	void SendFrameData(SOCKET parm_h_socket, char parm_id, void *parm_data, int parm_size);

	void AddEventList(const char *parm_string);

	LRESULT OnDataSocketMessage(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

};

#endif