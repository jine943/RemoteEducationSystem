#include "stdafx.h"
#include "MySocket.h"

MySocket::MySocket(int parm_dlg_id, CWnd* pParent) : CDialog(parm_dlg_id, pParent)
{
	mh_socket = INVALID_SOCKET;
	mp_event_list = NULL;
}

BEGIN_MESSAGE_MAP(MySocket, CDialog)
	//}}AFX_MSG_MAP
	ON_MESSAGE(LM_DATA_SOCKET_MESSAGE, OnDataSocketMessage)
END_MESSAGE_MAP()

void MySocket::DestroySocket(SOCKET parm_socket)
{
	LINGER l_linger = {TRUE, 0};
	setsockopt(parm_socket, SOL_SOCKET, SO_LINGER, (char FAR *)&l_linger, sizeof(l_linger));
	closesocket(parm_socket);
}


char *MySocket::ReadBodyData(SOCKET parm_h_socket, int parm_size)
{
	int read_size = 0, total_read_size = 0, retry_count = 0;

	char *p_body_data = new char[parm_size];
	while(total_read_size < parm_size){
		read_size = recv(parm_h_socket, p_body_data + total_read_size, parm_size - total_read_size, 0);
		if(read_size == SOCKET_ERROR){
			retry_count++;

			if(retry_count > 50) break;
			else Sleep(3);
		} else {
			retry_count = 0;
			total_read_size += read_size;
		}
	}
	return p_body_data;
}


void MySocket::SendFrameData(SOCKET parm_h_socket, char parm_id, void *parm_data, int parm_size)
{
	char *p_send_data = new char[parm_size + 4];

	*p_send_data = 37;
	*(unsigned short *)(p_send_data + 1) = parm_size;
	*(p_send_data + 3) = parm_id;

	if(parm_size > 0) memcpy(p_send_data + 4, parm_data, parm_size);
	send(parm_h_socket, p_send_data , parm_size + 4, 0);

	delete[] p_send_data;
}


void MySocket::ReadSocketData(SOCKET parm_socket, unsigned short parm_size, unsigned char parm_index, char *parm_body_data)
{

}

LRESULT MySocket::OnDataSocketMessage(WPARAM wParam, LPARAM lParam)
{
	unsigned char header[4];
	unsigned short body_size;
	char *p_body_data;

	SOCKET h_cur_socket = (SOCKET) wParam;
	
	if(FD_READ == WSAGETSELECTEVENT(lParam)){
		WSAAsyncSelect(h_cur_socket, m_hWnd, LM_DATA_SOCKET_MESSAGE, FD_CLOSE);		
		
		recv(h_cur_socket, (char *)header, 1, 0);
		if(37 == header[0]){
			recv(h_cur_socket, (char *)(header + 1), 3, 0);
			body_size = *(unsigned short *)(header + 1);

			if(body_size > 0){
				p_body_data = ReadBodyData(h_cur_socket, body_size);
				ReadSocketData(h_cur_socket, body_size, header[3], p_body_data);
				delete[] p_body_data;
		
			} else ReadSocketData(h_cur_socket, body_size, header[3], NULL);
		}
		WSAAsyncSelect(h_cur_socket, m_hWnd, LM_DATA_SOCKET_MESSAGE, FD_CLOSE | FD_READ);		
		
	} else {
		DestroySocket(h_cur_socket);
		wParam = INVALID_SOCKET;
	}
	return 1;
}



void MySocket::AddEventList(const char *parm_string)
{
	int index = mp_event_list->InsertString(-1, parm_string);
	mp_event_list->SetCurSel(index);
}
