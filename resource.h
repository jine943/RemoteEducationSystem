//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ImageClient.rc
//
#define IDD_IMAGECLIENT_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDD_INPUT_IP_DLG                129
#define IDD_SELECT_REMOTE_MODE          131
#define IDC_REQUEST_CHANGE_MODE         132
#define IDD_DIALOG                      133
#define IDD_GUEST_IP_DLG                134
#define IDD_IMAGESERVER_DIALOG          135
#define IDC_LIST1                       1000
#define IDC_EVENT_LIST                  1000
#define IDC_IP_LIST                     1000
#define IDC_CAPTURE_BTN                 1001
#define IDC_STOP_BTN                    1003
#define IDC_REQUIRE_GUEST               1004
#define IDC_IP_EDIT                     1005
#define IDC_HOST_BTN                    1008
#define IDC_GUEST_BTN                   1009
#define IDC_REQUIRE_CHANGE_MODE         1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
