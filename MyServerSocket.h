#ifndef _MY_SERVER_SOCKET_
#define _MY_SERVER_SOCKET_

#include "MySocket.h"

class MyServerSocket : public MySocket
{
protected:
	SOCKET mh_listen_socket;
	CString m_client_ip_address;

public:
	MyServerSocket(int parm_dlg_id, CWnd* pParent = NULL);
	// 열거형 상수
	enum { IDD = 0 };

	void ConnectClient(char *parm_ip, int parm_port);

	LRESULT OnAcceptSockProcess(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};
#endif