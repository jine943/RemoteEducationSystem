#include "stdafx.h"
#include "MyClientSocket.h"

MyClientSocket::MyClientSocket(int parm_dlg_id, CWnd* pParent) :  MySocket(parm_dlg_id, pParent)
{
	
}

BEGIN_MESSAGE_MAP(MyClientSocket, MySocket)
	ON_MESSAGE(LM_CONNECT_MESSAGE, OnConnectMessage)
END_MESSAGE_MAP()


void MyClientSocket::ConnectServer(char *parm_ip, int parm_port)
{
	mh_socket = socket(AF_INET, SOCK_STREAM, 0);

	struct sockaddr_in srv_addr;
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = inet_addr(parm_ip);
	srv_addr.sin_port = htons(parm_port);

	WSAAsyncSelect(mh_socket, m_hWnd, LM_CONNECT_MESSAGE, FD_CONNECT);
	connect(mh_socket, (LPSOCKADDR)&srv_addr, sizeof(srv_addr));
}

LRESULT MyClientSocket::OnConnectMessage(WPARAM wParam, LPARAM lParam)
{
	if(!WSAGETSELECTERROR(lParam)){
		WSAAsyncSelect(mh_socket, m_hWnd, LM_DATA_SOCKET_MESSAGE, FD_CLOSE | FD_READ);
		AddEventList("서버에 접속 했습니다.");
	} else {
		DestroySocket(mh_socket);
		mh_socket = INVALID_SOCKET;
		AddEventList("서버에 접속 할수 없습니다.");
	}
	return 0;
}

