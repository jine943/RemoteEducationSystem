#if !defined(AFX_REQUESTCHANGEMODEDLG_H__6DCFE3B8_F1B0_49B9_BFB4_D0A721FF2A3A__INCLUDED_)
#define AFX_REQUESTCHANGEMODEDLG_H__6DCFE3B8_F1B0_49B9_BFB4_D0A721FF2A3A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RequestChangeModeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// RequestChangeModeDlg dialog

class RequestChangeModeDlg : public CDialog
{
// Construction
public:
	RequestChangeModeDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(RequestChangeModeDlg)
	enum { IDD = IDC_REQUEST_CHANGE_MODE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RequestChangeModeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RequestChangeModeDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REQUESTCHANGEMODEDLG_H__6DCFE3B8_F1B0_49B9_BFB4_D0A721FF2A3A__INCLUDED_)
