// RequestChangeModeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ImageClient.h"
#include "RequestChangeModeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RequestChangeModeDlg dialog


RequestChangeModeDlg::RequestChangeModeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(RequestChangeModeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RequestChangeModeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void RequestChangeModeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RequestChangeModeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RequestChangeModeDlg, CDialog)
	//{{AFX_MSG_MAP(RequestChangeModeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RequestChangeModeDlg message handlers

void RequestChangeModeDlg::OnOK() 
{
	CDialog::OnOK();
}
