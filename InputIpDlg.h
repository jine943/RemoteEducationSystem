#if !defined(AFX_INPUTIPDLG_H__064E7B1F_4A49_434E_A81E_DCDA38116E4D__INCLUDED_)
#define AFX_INPUTIPDLG_H__064E7B1F_4A49_434E_A81E_DCDA38116E4D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InputIpDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// InputIpDlg dialog

class InputIpDlg : public CDialog
{
private: 
	char m_ip[MAX_IP_LEN];

public:
	InputIpDlg(CWnd* pParent = NULL);   // standard constructor
	char *GetIP();

// Dialog Data
	//{{AFX_DATA(InputIpDlg)
	enum { IDD = IDD_INPUT_IP_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(InputIpDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(InputIpDlg)
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INPUTIPDLG_H__064E7B1F_4A49_434E_A81E_DCDA38116E4D__INCLUDED_)
