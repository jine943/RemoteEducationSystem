// ImageServerDlg.h : header file
#if !defined(AFX_IMAGESERVERDLG_H__13E82C3C_BADF_42AD_A025_64D859925B71__INCLUDED_)
#define AFX_IMAGESERVERDLG_H__13E82C3C_BADF_42AD_A025_64D859925B71__INCLUDED_

#include "MyServerSocket.h"
#include "Receiver.h"
#include "Sender.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CImageServerDlg dialog

class CImageServerDlg : public MyServerSocket
{
private:
	Receiver m_receiver;
	Sender m_sender;
	// 0 : server 1 : client
	char m_mode;

public:
	CImageServerDlg(CWnd* pParent = NULL);	// standard constructor
	
	void ReadSocketData(SOCKET parm_socket, unsigned short parm_size, unsigned char parm_index, char *parm_body_data);	
	//void ChangeBitmap(unsigned short int *parm_bitmap, unsigned short parm_size);
	//void Encode32to16bits(int parm_width, int parm_height, unsigned char *parm_32_image, unsigned short int *parm_16_image);
	//char GetCurrentInputMode();
	//void SetCurrentInputMode(char parm_mode);

// Dialog Data
	//{{AFX_DATA(CImageServerDlg)
	enum { IDD = IDD_IMAGESERVER_DIALOG };
	CListBox	m_event_list;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImageServerDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CImageServerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnClose();
	afx_msg void OnRequireChangeMode();
	afx_msg void OnCaptureBtn();
	afx_msg void OnStopBtn();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG
	LRESULT ReqireChangeMode(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMAGESERVERDLG_H__13E82C3C_BADF_42AD_A025_64D859925B71__INCLUDED_)
