// ImageServerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ImageClient.h"
#include "ImageServerDlg.h"
#include "RequestChangeModeDlg.h"


#include "imm.h"
#pragma comment (lib, "imm32.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImageServerDlg dialog

CImageServerDlg::CImageServerDlg(CWnd* pParent): MyServerSocket(CImageServerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CImageServerDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	mh_socket = INVALID_SOCKET;
	m_mode = GUEST_MODE;
}

void CImageServerDlg::DoDataExchange(CDataExchange* pDX)
{
	MyServerSocket::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CImageServerDlg)
	DDX_Control(pDX, IDC_EVENT_LIST, m_event_list);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CImageServerDlg, MyServerSocket)
	//{{AFX_MSG_MAP(CImageServerDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_REQUIRE_CHANGE_MODE, OnRequireChangeMode)
	ON_BN_CLICKED(IDC_CAPTURE_BTN, OnCaptureBtn)
	ON_BN_CLICKED(IDC_STOP_BTN, OnStopBtn)
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
	ON_MESSAGE(LM_REQUIRE_CHANGE_MODE, ReqireChangeMode)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImageServerDlg message handlers


LRESULT CImageServerDlg::ReqireChangeMode(WPARAM wParam, LPARAM lParam)
{
	char flag = 0;

	if(0 == lParam){
		RequestChangeModeDlg ins_dlg;
		if(IDOK == ins_dlg.DoModal()){
			flag = 1;
		}
	}
	
	if(flag || lParam){
		if(GUEST_MODE == m_mode){
			m_mode = HOST_MODE;

			GetDlgItem(IDC_CAPTURE_BTN)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_STOP_BTN)->ShowWindow(SW_SHOW);
			GetDlgItem(IDOK)->ShowWindow(SW_SHOW);
			GetDlgItem(IDCANCEL)->ShowWindow(SW_SHOW);
			m_event_list.ShowWindow(SW_SHOW);

			SetWindowPos(NULL, 0, 0, 300, 150, NULL);

			ModifyStyle(NULL, WS_CAPTION | WS_DLGFRAME | WS_SYSMENU);
			
		} else {
			m_mode = GUEST_MODE;

			// 버튼 생성
			GetDlgItem(IDC_CAPTURE_BTN)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_STOP_BTN)->ShowWindow(SW_HIDE);
			GetDlgItem(IDOK)->ShowWindow(SW_HIDE);
			GetDlgItem(IDCANCEL)->ShowWindow(SW_HIDE);
			m_event_list.ShowWindow(SW_HIDE);

			// 리사이즈가 안됨
			SetWindowPos(NULL, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), NULL);

			ModifyStyle(WS_CAPTION | WS_DLGFRAME | WS_SYSMENU, NULL);

		}
		if(lParam == 0) SendFrameData(mh_socket, NM_RESULT_CHANGE_MODE, NULL, 0);
	}
	return 1;	
}

BOOL CImageServerDlg::OnInitDialog()
{
	MyServerSocket::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	m_receiver.InitialData(this);

	ConnectClient("58.233.158.53", 26000);

	m_receiver.ResizeWindow(this);

	mp_event_list = &m_event_list;
	AddEventList("서비스가 시작되었습니다.");
	

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.


void CImageServerDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	if (IsIconic())
	{
		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);

	} else m_receiver.InvalidateImage(&dc);
}

void CImageServerDlg::ReadSocketData(SOCKET parm_socket, unsigned short parm_size, unsigned char parm_index, char *parm_body_data)
{
	if(NM_SEND_STEP_SIZE == parm_index){

		m_receiver.MakeReceiveData((unsigned short int *)parm_body_data, this);
		SendFrameData(parm_socket, NM_REQUEST_STEP_INDEX, NULL, 0);

	// 날아오는 그림의 행열을 받는다
	} else if(NM_NEXT_STEP_INDEX == parm_index){

		m_receiver.SetNextStepIndex((unsigned short int *)parm_body_data);
		SendFrameData(parm_socket, NM_REQUEST_STEP_DATA, NULL, 0);

	// 100 분할 이미지에서 중간 1KB
	} else if(NM_SEND_STEP_DATA == parm_index){
		
		m_receiver.AddStepImage(parm_body_data, parm_size);
		//memcpy(m_receiver.GetReceiveStepData(), parm_body_data, parm_size);
		//m_receiver.SetCurrentFrameSize(parm_size);

		// 4번 메시지. 1KB 받았다 더 보내
		SendFrameData(parm_socket, NM_REQUEST_STEP_DATA, NULL, 0);

	// 100 분할 이미지에서 마지막 1KB
	} else if(NM_SEND_LAST_STEP_DATA == parm_index){

		m_receiver.AddStepImage(parm_body_data, parm_size);
		//memcpy(m_receiver.GetReceiveStepData(), parm_body_data, parm_size);
		//m_receiver.SetCurrentFrameSize(parm_size);

		m_receiver.DecodeStepImage();

		SendFrameData(parm_socket, NM_REQUEST_STEP_INDEX, NULL, 0);

	} else if(NM_INVALIDATE_IMAGE == parm_index){
		Invalidate();

	} else if(NM_REQUIRE_CHANGE_MODE == parm_index){
		PostMessage(LM_REQUIRE_CHANGE_MODE, 0, 0);

	} else if(NM_RESULT_CHANGE_MODE == parm_index){
		PostMessage(LM_REQUIRE_CHANGE_MODE, 0, 1);

	}
}


// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CImageServerDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CImageServerDlg::OnDestroy() 
{
	MyServerSocket::OnDestroy();
	
	mh_socket = INVALID_SOCKET;
}

// 마우스 좌표 클라이언트에게 전송
void CImageServerDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if(INVALID_SOCKET != mh_socket){
		SendFrameData(mh_socket, 20, &point, sizeof(point));
	}

	MyServerSocket::OnMouseMove(nFlags, point);
}

void CImageServerDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if(INVALID_SOCKET != mh_socket){
		SendFrameData(mh_socket, 21, &point, sizeof(point));
	}
	
	MyServerSocket::OnLButtonDown(nFlags, point);
}

void CImageServerDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if(INVALID_SOCKET != mh_socket){
		SendFrameData(mh_socket, 22, &point, sizeof(point));
	}	
	
	MyServerSocket::OnLButtonUp(nFlags, point);
}


void CImageServerDlg::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	if(INVALID_SOCKET != mh_socket && GUEST_MODE == g_mode){
		SendFrameData(mh_socket, 25, &point, sizeof(point));
	}
	
	MyServerSocket::OnLButtonDblClk(nFlags, point);
}

void CImageServerDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if(INVALID_SOCKET != mh_socket){
		char temp = nChar;
		CString str;
		str.Format("키보드 입력 값 : %d", temp);
		MessageBox(str);
		SendFrameData(mh_socket, 23, &temp, sizeof(char));
	}	
	
	MyServerSocket::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CImageServerDlg::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	MyServerSocket::OnKeyUp(nChar, nRepCnt, nFlags);
}

void CImageServerDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	
	MyServerSocket::OnClose();
}


BOOL CImageServerDlg::PreTranslateMessage(MSG* pMsg) 
{
	if(GUEST_MODE == g_mode){
		if(WM_KEYDOWN == pMsg->wParam){
			unsigned char keyboard_data[2];
			char ret_val = m_receiver.ReadKeyEvent(pMsg, keyboard_data);
			if(ret_val){
				SendFrameData(mh_socket, 30, keyboard_data, sizeof(keyboard_data));
				if(2 == ret_val) return 1; 
			}
		}
	}
	// 메시지 돌려주기 
	return MyServerSocket::PreTranslateMessage(pMsg);
}


/*void CImageServerDlg::ChangeBitmap(unsigned short int *parm_bitmap, unsigned short parm_size)
{
	unsigned char red, green, blue;
	int pixcel_count = parm_size / 2, x;
	unsigned int *p_image = new unsigned int[pixcel_count];
	unsigned int *p_image_pos = p_image;

	for(x = 0; x < pixcel_count ; x++){
		red = (*parm_bitmap & 0x1F) * 8;
		green = ((*parm_bitmap >> 5) & 0x1F) * 8;
		blue = ((*parm_bitmap >> 11) & 0x1F) * 8;

		*p_image_pos++ = red | (green << 8) | (blue << 16);
		parm_bitmap++;
	}
	memcpy(mp_image + m_recv_image_size, p_image, parm_size * 2);
	m_recv_image_size += parm_size * 2;

	delete[] p_image;
}
*/

void CImageServerDlg::OnRequireChangeMode() 
{
	SendFrameData(mh_socket, NM_REQUIRE_CHANGE_MODE, NULL, 0);	
}

void CImageServerDlg::OnCaptureBtn() 
{
	// TODO: Add your control notification handler code here
	
}

void CImageServerDlg::OnStopBtn() 
{
	// TODO: Add your control notification handler code here
	
}

