#ifndef _MY_BI_SOCKET_
#define _MY_BI_SOCKET_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MySocket.h"

class MyBiSocket : public MySocket
{
protected:
	SOCKET mh_listen_socket;
	CString m_client_ip_address;

public:
	MyBiSocket(int parm_dlg_id, CWnd* pParent = NULL);

// Dialog Data
	//{{AFX_DATA(MyBiSocket)
	enum { IDD = 0 };
	//}}AFX_DATA

	void Connect(int parm_port, char *parm_ip = NULL);
	void ConnectClient(char *parm_ip, int parm_port); // server
	void ConnectServer(char *parm_ip, int parm_port); // client

	LRESULT OnConnectMessage(WPARAM wParam, LPARAM lParam);
	LRESULT OnAcceptSockProcess(WPARAM wParam, LPARAM lParam);

	//{{AFX_MSG(MyBiSocket)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

#endif