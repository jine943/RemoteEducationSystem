#include "stdafx.h"
#include "Sender.h"

Sender::Sender()
{
	mp_mem_dc = NULL;
	mp_src_dc = NULL;
	mp_mem_bitmap = NULL;
	mp_src_bitmap = NULL;
	m_cur_send_size = 0;
}

Sender::Sender(CWnd *parm_base_wnd)
{
	InitialData(parm_base_wnd);	
}

Sender::~Sender()
{
	DeleteData();
}


void Sender::InitialData(CWnd *parm_base_wnd)
{
	CClientDC dc(parm_base_wnd);

	mp_mem_dc = new CDC();
	mp_src_dc = new CDC();

	mp_mem_dc->CreateCompatibleDC(&dc);
	mp_src_dc->CreateCompatibleDC(&dc);

	// 스크린 전체 가로 세로
	m_screen_width = GetSystemMetrics(SM_CXSCREEN);
	m_screen_height = GetSystemMetrics(SM_CYSCREEN);

	// 스크린을 분할한 것의 가로 세로
	m_screen_step[0] = m_screen_width / MAX_STEP_COUNT;
	m_screen_step[1] = m_screen_height / MAX_STEP_COUNT;
	
	mp_mem_bitmap = new CBitmap();
	mp_src_bitmap = new CBitmap();
	
	mp_mem_bitmap->CreateCompatibleBitmap(&dc, m_screen_step[0], m_screen_step[1]);
	mp_src_bitmap->CreateCompatibleBitmap(&dc, m_screen_width, m_screen_height);
	
	memset(m_send_flag, 0, MAX_STEP_COUNT * MAX_STEP_COUNT * sizeof(unsigned int));
	
	for (int y = 0; y < MAX_STEP_COUNT; y++){
		for (int x = 0; x < MAX_STEP_COUNT; x++){
			mp_send_image[y][x] = new unsigned char[m_screen_step[0] * m_screen_step[1] * 4];
			mp_send_encode_image[y][x] = new unsigned char[m_screen_step[0] * m_screen_step[1] * 4];
			memset(mp_send_image[y][x], 0, m_screen_step[0] * m_screen_step[1] * 4);
			m_send_flag[y][x] = 0;
		}
	}
	mp_check_image = new unsigned char[m_screen_step[0] * m_screen_step[1] * 4];
	m_cur_send_size = 0;
}

void Sender::DeleteData()
{
	if(NULL != mp_mem_dc){
		mp_mem_dc->DeleteDC();
		delete mp_mem_dc;
		mp_mem_dc = NULL;
	}

	if(NULL != mp_src_dc){
		mp_src_dc->DeleteDC();
		delete mp_src_dc;
		mp_src_dc = NULL;
	}
	
	if(NULL != mp_mem_bitmap){
		mp_mem_bitmap->DeleteObject();
		delete mp_mem_bitmap;
		mp_mem_bitmap = NULL;
	}

	if(NULL != mp_src_bitmap){
		mp_src_bitmap->DeleteObject();
		delete mp_src_bitmap;
		mp_src_bitmap = NULL;
	}	
	
	if(NULL != mp_check_image){
		delete[] mp_check_image;
		mp_check_image = NULL;
	
		for (int y = 0; y < MAX_STEP_COUNT; y++){
			for (int x = 0; x < MAX_STEP_COUNT; x++){
				delete[] mp_send_image[y][x];
				delete[] mp_send_encode_image[y][x];
			}
		}
	}	
}


//  이사님 코드
unsigned int Sender::Encode32to16bits(unsigned int parm_width, unsigned int parm_height, unsigned char *parm_32_image, unsigned char *parm_16_image)
{
	unsigned char temp_r, temp_g, temp_b, current_encode_color_count = 1, *p_save_pos = parm_16_image;
	unsigned short temp_color, current_encode_color = 0;
	unsigned short *p_pos = (unsigned short *)mp_check_image;
	int count = parm_height * parm_width, i;

	temp_r = *parm_32_image++;  // red
	temp_g = *parm_32_image++;  // green
	temp_b = *parm_32_image++;  // blue
	parm_32_image++;            // alpha
	current_encode_color = (temp_b >> 3) | ((temp_g >> 3) << 6) | ((temp_r >> 3) << 11);

	*p_pos++ = (temp_b >> 3) | ((temp_g >> 3) << 6) | ((temp_r >> 3) << 11);

	for(i = 1; i < count; i++){
		temp_r = *parm_32_image++;  // red
		temp_g = *parm_32_image++;  // green
		temp_b = *parm_32_image++;  // blue
		parm_32_image++;            // alpha

		temp_color = (temp_b >> 3) | ((temp_g >> 3) << 6) | ((temp_r >> 3) << 11);

		if(current_encode_color == temp_color){
			if(current_encode_color_count == 255){
				*parm_16_image++ = current_encode_color_count;
				*(unsigned short *)parm_16_image = current_encode_color;
				parm_16_image += 2;

				current_encode_color_count = 1;
			} else current_encode_color_count++;

		} else {
			*parm_16_image++ = current_encode_color_count;
			*(unsigned short *)parm_16_image = current_encode_color;
			parm_16_image += 2;

			current_encode_color = temp_color;
			current_encode_color_count = 1;
		}
		*p_pos++ = temp_color;
	}

	*parm_16_image++ = current_encode_color_count;
	*(unsigned short *)parm_16_image = current_encode_color;
	parm_16_image += 2;

	unsigned int encode_size = parm_16_image - p_save_pos;
	if(encode_size > parm_width * parm_height * 2){
		memcpy(p_save_pos, mp_check_image, parm_width * parm_height * 2);
		return 0;
	}
	return encode_size;
}

/* 
unsigned int Sender::Encode32to16bits(int parm_width, int parm_height, unsigned char *parm_32_image, unsigned char *parm_16_image)
{
	unsigned char red, green, blue;
	int count = parm_width * parm_height;
	unsigned short int temp_color, encode_color = 0, encode_color_count = 1;
	// 32bit 이미지
	unsigned short int *p_pos = (unsigned short int *)mp_check_image;
	unsigned char *p_save_pos = parm_16_image;
	
	red = *parm_32_image++;
	green = *parm_32_image++;
	blue = *parm_32_image++;
	parm_32_image++;

	encode_color = (blue >> 3) | ((green >> 3) << 6) | ((red >> 3) << 11);

	*p_pos++ = (blue >> 3) | ((green >> 3) << 6) | ((red >> 3) << 11);

	for(int i = 1; i < count; i++){
		red = *parm_32_image++;
		green = *parm_32_image++;
		blue = *parm_32_image++;
		parm_32_image++;

		temp_color = (blue >> 3) | ((green >> 3) << 6) | ((red >> 3) << 11);
		
		if(encode_color == temp_color){
			encode_color_count++;
		} else {
			if(encode_color_count > 255){
				encode_color |= 0x0020;
				*(unsigned short *)parm_16_image = encode_color;
				parm_16_image += 2;
				*(unsigned short *)parm_16_image = encode_color_count;
				parm_16_image += 2;
			} else {
				*(unsigned short *)parm_16_image = encode_color;
				parm_16_image += 2;
				*parm_16_image++ = (unsigned char)encode_color_count;
			}
			encode_color = temp_color;
			encode_color_count = 1;
		}
		*p_pos++ = temp_color;
	}
	// 마지막 컬러에 대하여 저장
	if( encode_color_count > 255){
		temp_color |= 0x0020;
		*(unsigned short *)parm_16_image = encode_color;
		parm_16_image += 2;
		*(unsigned short *)parm_16_image = encode_color_count;
		parm_16_image += 2;
	} else {
		*(unsigned short *)parm_16_image = encode_color;
		parm_16_image += 2;
		*parm_16_image++ = (unsigned char)encode_color_count;
	}

	int encode_size = parm_16_image - p_save_pos;
	if(encode_size >= parm_width * parm_height * 2){
		memcpy(p_save_pos, mp_check_image, parm_width * parm_height * 2);
		return 0;
	}
	return encode_size;
	
}

//0503진희 코드
// RGB(2byte) + 동일 색상 수(1 or 2 byte)
// 16bit 색상에서 사용되지 않는 6번째 bit를 활용하여 색상 수를 저장하는 메모리 크기를 구분한다
unsigned int  Sender::Encode32to16bits(int parm_width, int parm_height, unsigned char *parm_32_image, unsigned char *parm_16_image)
{
	unsigned char red, green, blue, short_flag = 0;
	int count = parm_width * parm_height;
	unsigned short int temp_color, encode_color = 0, encode_color_count = 1;
	// 32bit 이미지
	unsigned short int *p_pos = (unsigned short int *)mp_check_image;
	unsigned char *p_save_pos = parm_16_image;
	
	red = *parm_32_image++;
	green = *parm_32_image++;
	blue = *parm_32_image++;
	parm_32_image++;

	encode_color = ((red >> 3) << 11) | ((green >> 3) << 6) | (blue >> 3);
	*p_pos++ = encode_color;

	for(int i = 1; i < count; i++){
		red = *parm_32_image++;
		green = *parm_32_image++;
		blue = *parm_32_image++;
		parm_32_image++;

		temp_color = ((red >> 3) << 11) | ((green >> 3) << 6) | (blue >> 3);

		if(encode_color == temp_color){
			// 색상 수를 저장할 공간을 2byte 가 필요한 경우 short_flag 세팅
			if(255 == encode_color_count){
				// 1 로 세팅
				encode_color |= 0x0020;
				short_flag = 1;
			//*parm_16_image++ = encode_color_count;
			//*(unsigned short *)parm_16_image = encode_color;
			//parm_16_image += 2;
			//encode_color_count = 1;
			} 
			encode_color_count++;
		} else {
			// short_flag 에 따라 메모리 공간 달리하기
			if(1 == short_flag){
				*(unsigned short *)parm_16_image = encode_color;
				parm_16_image += 2;
				*(unsigned short *)parm_16_image = encode_color_count;
				parm_16_image += 2;
				short_flag = 0;
			} else {
				// 0 으로 세팅하기 위해 0xffdf
				// 0xdf로 하면 에러
				encode_color &= 0xffdf;
				*(unsigned short *)parm_16_image = encode_color;
				parm_16_image += 2;
				*parm_16_image++ = (unsigned char)encode_color_count;				
			}
			encode_color = temp_color;
			encode_color_count = 1;
		}	
		*p_pos++ = temp_color;
	}
	// 마지막 컬러에 대하여 저장
	if(1 == short_flag){
		*(unsigned short *)parm_16_image = encode_color;
		parm_16_image += 2;
		*(unsigned short *)parm_16_image = encode_color_count;
		parm_16_image += 2;
	} else {
		encode_color &= 0xffdf;
		*(unsigned short *)parm_16_image = encode_color;
		parm_16_image += 2;
		*parm_16_image++ = (unsigned char)encode_color_count;
	}

	int encode_size = parm_16_image - p_save_pos;
	if(encode_size >= parm_width * parm_height * 2){
		memcpy(p_save_pos, mp_check_image, parm_width * parm_height * 2);
		return 0;
	}
	return encode_size;
	
}
*/

void Sender::MakeSendData()
{
	CWindowDC screen_dc(NULL);
	CBitmap *p_old_src_bmp = mp_src_dc->SelectObject(mp_src_bitmap);
	CBitmap *p_old_bmp = mp_mem_dc->SelectObject(mp_mem_bitmap);

	mp_src_dc->BitBlt(0, 0, m_screen_width, m_screen_height, &screen_dc, 0, 0, SRCCOPY);

	// 다른 부분 
	m_total_size = 0;
	int screen_size = m_screen_step[0] * m_screen_step[1] * 4, x, y;

	//FILE *p_file = fopen("debug_rc.txt", "a+t");
	//if(p_file != NULL) fprintf(p_file, "New Frame!!\n");
	
	// 100 분할 캡쳐
	for(y = 0; y < MAX_STEP_COUNT; y++){
		for(x = 0; x < MAX_STEP_COUNT; x++){
			// 100 분할씩 정보를 가져온다
			mp_mem_dc->BitBlt(0, 0, m_screen_step[0], m_screen_step[1], 
				              mp_src_dc, m_screen_step[0] * x, m_screen_step[1] * y, SRCCOPY);
			mp_mem_bitmap->GetBitmapBits(screen_size, mp_check_image);
			
			if(memcmp(mp_check_image, mp_send_image[y][x], screen_size)){							
				memcpy(mp_send_image[y][x], mp_check_image, screen_size);
				                                     
				m_send_flag[y][x] = Encode32to16bits(m_screen_step[0], m_screen_step[1], mp_send_image[y][x], mp_send_encode_image[y][x]);
				if(0 == m_send_flag[y][x]) m_send_flag[y][x]++;

				//if(p_file != NULL) fprintf(p_file, "%02d, %02d : %d\n", y, x, m_send_flag[y][x]);

			} else m_send_flag[y][x] = 0;
		}
	}

//	if(p_file != NULL) fclose(p_file);

	
	mp_mem_dc->SelectObject(p_old_bmp);
	mp_src_dc->SelectObject(p_old_src_bmp);

	m_send_index[0] = 0; // x
	m_send_index[1] = 0; // y
}

unsigned short int *Sender::GetStepSize()
{
	return m_screen_step;
}


unsigned short int *Sender::NextStepIndex()
{
	while(0 == m_send_flag[m_send_index[1]][m_send_index[0]]){
		m_send_index[0]++;
		if(MAX_STEP_COUNT == m_send_index[0]){
			m_send_index[0] = 0;
			m_send_index[1]++;
			if(MAX_STEP_COUNT == m_send_index[1]) break;
		}
	}
	if(MAX_STEP_COUNT == m_send_index[1]) return NULL;

	m_cur_send_size = 0;
	return m_send_index;
}

void Sender::CalTotalSize()
{
	if(1 == m_send_flag[m_send_index[1]][m_send_index[0]]){
		m_total_size = m_screen_step[0] * m_screen_step[1] * 2;
	} else {
		m_total_size = m_send_flag[m_send_index[1]][m_send_index[0]];
	}
}

char Sender::IsExistNextStep()
{
	// 더 보낼것 있으면 0 없으면 1
	if (m_total_size <= MAX_PACKET_SIZE || m_cur_send_size >= (unsigned int)(m_total_size - MAX_PACKET_SIZE)) return 0;

	// 더 보낼것 있으면 1 없으면 0
	//return (m_total_size <= MAX_PACKET_SIZE || m_cur_send_size >= (unsigned int)(m_total_size - MAX_PACKET_SIZE));
	//if(m_total_size <= MAX_PACKET_SIZE || m_cur_send_size >= (unsigned int)(m_total_size - MAX_PACKET_SIZE)) return 0;
	return 1;
}

unsigned char *Sender::GetSendStepData()
{
	return mp_send_encode_image[m_send_index[1]][m_send_index[0]] + m_cur_send_size;
}

unsigned int Sender::GetCurrentFrameSize()
{
	unsigned int temp_size = m_total_size - m_cur_send_size;
	if(temp_size > MAX_PACKET_SIZE) return MAX_PACKET_SIZE;
	
	return temp_size;
}

void Sender::NextFrame()
{
	if(IsExistNextStep() == 0){
		m_send_index[0]++;
		if(m_send_index[0] == 10){
			m_send_index[0] = 0;
			m_send_index[1]++;
		}	
	} else m_cur_send_size += MAX_PACKET_SIZE;
}


void Sender::WriteMouseEvent(int *parm_data, UINT parm_mode)
{
	if(MOUSEEVENTF_WHEEL == parm_mode){
		mouse_event(parm_mode | MOUSEEVENTF_ABSOLUTE, parm_data[0], parm_data[1], *((short *)parm_data + 4), 0);
	} else {
		int x = parm_data[0] * 65535 / m_screen_width;
		int y = parm_data[1] * 65535 / m_screen_height;
		// 마우스의 위치를 화면 0, 0 좌표로부터 절대적인 위치로 한다
		mouse_event(parm_mode | MOUSEEVENTF_ABSOLUTE, x, y, 0, 0);
	}
}

void Sender::WriteKeyEvent(unsigned char *parm_data)
{
	if(parm_data[1]) keybd_event(VK_SHIFT, 0, 0, 0);
	
	keybd_event(parm_data[0], parm_data[0], 0, 0);
	keybd_event(parm_data[0], parm_data[0], KEYEVENTF_KEYUP, 0);

	if(parm_data[1]) keybd_event(VK_SHIFT, 0, KEYEVENTF_KEYUP, 0);
}