// SelectRemoteModeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ImageClient.h"
#include "SelectRemoteModeDlg.h"
#include "ImageClientDlg.h"
#include "ImageServerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SelectRemoteModeDlg dialog


SelectRemoteModeDlg::SelectRemoteModeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SelectRemoteModeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SelectRemoteModeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_mode = -1;
}


void SelectRemoteModeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SelectRemoteModeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SelectRemoteModeDlg, CDialog)
	//{{AFX_MSG_MAP(SelectRemoteModeDlg)
	ON_BN_CLICKED(IDC_HOST_BTN, OnHostBtn)
	ON_BN_CLICKED(IDC_GUEST_BTN, OnGuestBtn)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SelectRemoteModeDlg message handlers


void SelectRemoteModeDlg::OnGuestBtn() 
{
	m_mode = GUEST_MODE;
	OnOK();
}


void SelectRemoteModeDlg::OnHostBtn() 
{
	m_mode = HOST_MODE;
	// PostMessage(WM_COMMAND, IDOK);
	OnOK();
}


char SelectRemoteModeDlg::GetRemoteMode()
{
	return m_mode;
}
