// ImageClientDlg.h : header file
//

#if !defined(AFX_IMAGECLIENTDLG_H__9C9304D9_F972_4173_BC57_0FB6A22B2026__INCLUDED_)
#define AFX_IMAGECLIENTDLG_H__9C9304D9_F972_4173_BC57_0FB6A22B2026__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Sender.h"
#include "Receiver.h"
#include "MyBiSocket.h"

/////////////////////////////////////////////////////////////////////////////
// CImageClientDlg dialog

class CImageClientDlg : public MyBiSocket
{
private:
	Sender m_sender;
	Receiver m_receiver;

	char m_start_flag;
	char m_ip[MAX_IP_LEN];


public:
	CImageClientDlg(CWnd* pParent = NULL);	// standard constructor

	virtual void ReadSocketData(SOCKET parm_socket, unsigned short parm_size, unsigned char parm_index, char *parm_body_data);
	void SetIP(const char *parm_ip);
	void ChangeDisplayProperty(char parm_mode);


// Dialog Data
	//{{AFX_DATA(CImageClientDlg)
	enum { IDD = IDD_IMAGECLIENT_DIALOG };
	CListBox	m_event_list;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImageClientDlg)
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CImageClientDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnCaptureBtn();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnStopBtn();
	afx_msg void OnRequireGuest();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	//}}AFX_MSG
	LRESULT ReqireChangeMode(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMAGECLIENTDLG_H__9C9304D9_F972_4173_BC57_0FB6A22B2026__INCLUDED_)
