#include "stdafx.h"
#include "ImageClient.h"
#include "MyBiSocket.h"
#include "InputIpDlg.h"
#include "GuestIpDlg.h"

MyBiSocket::MyBiSocket(int parm_dlg_id, CWnd* pParent) : MySocket(parm_dlg_id, pParent)
{
	mh_listen_socket = INVALID_SOCKET;
}


BEGIN_MESSAGE_MAP(MyBiSocket, MySocket)
	//{{AFX_MSG_MAP(MyBiSocket)
	//}}AFX_MSG_MAP
 	ON_MESSAGE(LM_ACCEPT_MESSAGE, OnAcceptSockProcess)
	ON_MESSAGE(LM_CONNECT_MESSAGE, OnConnectMessage)
END_MESSAGE_MAP()


void MyBiSocket::Connect(int parm_port, char *parm_ip)
{
	char ip[MAX_IP_LEN];
	CString str;
	
	if(HOST_MODE == g_mode){
		InputIpDlg ip_dlg;
		if(IDOK == ip_dlg.DoModal()){
			ConnectServer(ip_dlg.GetIP(), parm_port);
			
			str.Format("Student 모드입니다.");
			SetWindowText(str);
		} else EndDialog(IDOK);
		
	} else {
		GuestIpDlg ins_dlg;
		if(IDOK == ins_dlg.DoModal()){
			strcpy(ip, ins_dlg.GetServerIp());
			ConnectClient(ip, parm_port);
			
			str.Format("Teacher 모드입니다.");
			SetWindowText(str);
		} else {
			EndDialog(IDOK);
		}
	}
}

void MyBiSocket::ConnectClient(char *parm_ip, int parm_port)
{
	mh_listen_socket = socket(AF_INET, SOCK_STREAM, 0);

	struct sockaddr_in srv_addr;
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = inet_addr(parm_ip);
	srv_addr.sin_port = htons(parm_port);

	bind(mh_listen_socket, (LPSOCKADDR)&srv_addr, sizeof(struct sockaddr_in));
	listen(mh_listen_socket, 1);

	WSAAsyncSelect(mh_listen_socket, this->m_hWnd, LM_ACCEPT_MESSAGE, FD_ACCEPT);
}


LRESULT MyBiSocket::OnAcceptSockProcess(WPARAM wParam, LPARAM lParam)
{
	if(INVALID_SOCKET == mh_socket){
		sockaddr_in client_addr;
		int client_addr_size = sizeof(sockaddr_in);
		
		mh_socket = accept((SOCKET)wParam, (LPSOCKADDR)&client_addr, &client_addr_size);
		m_client_ip_address = inet_ntoa(client_addr.sin_addr);

		WSAAsyncSelect(mh_socket, m_hWnd, LM_DATA_SOCKET_MESSAGE, FD_READ | FD_CLOSE);

		AddEventList("새로운 클라이언트가 접속했습니다 -> " + m_client_ip_address);
	}
	return 1;
}


void MyBiSocket::ConnectServer(char *parm_ip, int parm_port)
{
	mh_socket = socket(AF_INET, SOCK_STREAM, 0);

	struct sockaddr_in srv_addr;
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = inet_addr(parm_ip);
	srv_addr.sin_port = htons(parm_port);

	WSAAsyncSelect(mh_socket, m_hWnd, LM_CONNECT_MESSAGE, FD_CONNECT);
	connect(mh_socket, (LPSOCKADDR)&srv_addr, sizeof(srv_addr));
}

LRESULT MyBiSocket::OnConnectMessage(WPARAM wParam, LPARAM lParam)
{
	if(!WSAGETSELECTERROR(lParam)){
		WSAAsyncSelect(mh_socket, m_hWnd, LM_DATA_SOCKET_MESSAGE, FD_CLOSE | FD_READ);
		AddEventList("서버에 접속 했습니다.");
	} else {
		DestroySocket(mh_socket);
		mh_socket = INVALID_SOCKET;
		AddEventList("서버에 접속 할수 없습니다.");
	}
	return 0;
}
