#ifndef _SENDER_H_
#define _SENDER_H_

#define MAX_STEP_COUNT	10

class Sender
{
private:
	CDC *mp_mem_dc, *mp_src_dc;
	CBitmap *mp_mem_bitmap, *mp_src_bitmap;

	int m_screen_width, m_screen_height;
	unsigned short int m_screen_step[2]; 
	// 0th -> Width , 0th -> Height 
	unsigned int m_total_size, m_cur_send_size;

	// m_send_index 100 분할 이미지의 행열
	// m_current_index 분할 이미지를 1kB 씩 보낸 횟수
	unsigned short int m_send_index[2], m_current_index;
	// 보낸 이미지 

	unsigned char *mp_send_image[MAX_STEP_COUNT][MAX_STEP_COUNT];
	unsigned char *mp_send_encode_image[MAX_STEP_COUNT][MAX_STEP_COUNT];
	unsigned char *mp_check_image;

	unsigned int m_send_flag[MAX_STEP_COUNT][MAX_STEP_COUNT];

public:
	Sender();
	Sender(CWnd *parm_base_wnd);
	~Sender();
	void InitialData(CWnd *parm_base_wnd);
	void DeleteData();

	void MakeSendData();

	unsigned int Encode32to16bits(unsigned int parm_width, unsigned int parm_height, unsigned char *parm_32_image, unsigned char *parm_16_image);

	unsigned short int *GetStepSize();
	unsigned char *GetSendStepData();
	unsigned int GetCurrentFrameSize();
	void NextFrame();

	void CalTotalSize();
	char IsExistNextStep();

	void WriteMouseEvent(int *parm_data, UINT parm_mode);
	void WriteKeyEvent(unsigned char *parm_data);

	unsigned short int *NextStepIndex();
};

#endif