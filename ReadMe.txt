========================================================================
    Windows PC 간 화면 공유/원격 제어를 통한 온라인 교육 Application
========================================================================

1) 진행기간
    2014.03 - 2014.06
    
2) 주요내용
    Windows PC 간 화면 공유/원격 제어를 통한 온라인 교육 Application
    
3)특징
    - 화면 압축 알고리즘 구현하여 8Mbyte 크기 화면을 평균 300byte ~ 4Kbyte 로 압축
    - TCP/IP기반 화면 전송 프로토콜 구현
    - 다형성을 적용한 서버 App과 클라이언트 App을 통합
    
4)사용한 Skill 또는 지식
    - C/C++, 마우스 및 키보드 원격 제어, TCP/IP 서버 및 클라이언트 통신 구현
5)성과/결과
    - PC 화면 1대 N 공유하는 교육시스템 구현

========================================================================
       MICROSOFT FOUNDATION CLASS LIBRARY : ImageClient
========================================================================


AppWizard has created this ImageClient application for you.  This application
not only demonstrates the basics of using the Microsoft Foundation classes
but is also a starting point for writing your application.

This file contains a summary of what you will find in each of the files that
make up your ImageClient application.

ImageClient.dsp
    This file (the project file) contains information at the project level and
    is used to build a single project or subproject. Other users can share the
    project (.dsp) file, but they should export the makefiles locally.

ImageClient.h
    This is the main header file for the application.  It includes other
    project specific headers (including Resource.h) and declares the
    CImageClientApp application class.

ImageClient.cpp
    This is the main application source file that contains the application
    class CImageClientApp.

ImageClient.rc
    This is a listing of all of the Microsoft Windows resources that the
    program uses.  It includes the icons, bitmaps, and cursors that are stored
    in the RES subdirectory.  This file can be directly edited in Microsoft
	Visual C++.

ImageClient.clw
    This file contains information used by ClassWizard to edit existing
    classes or add new classes.  ClassWizard also uses this file to store
    information needed to create and edit message maps and dialog data
    maps and to create prototype member functions.

res\ImageClient.ico
    This is an icon file, which is used as the application's icon.  This
    icon is included by the main resource file ImageClient.rc.

res\ImageClient.rc2
    This file contains resources that are not edited by Microsoft 
	Visual C++.  You should place all resources not editable by
	the resource editor in this file.




/////////////////////////////////////////////////////////////////////////////

AppWizard creates one dialog class:

ImageClientDlg.h, ImageClientDlg.cpp - the dialog
    These files contain your CImageClientDlg class.  This class defines
    the behavior of your application's main dialog.  The dialog's
    template is in ImageClient.rc, which can be edited in Microsoft
	Visual C++.


/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named ImageClient.pch and a precompiled types file named StdAfx.obj.

Resource.h
    This is the standard header file, which defines new resource IDs.
    Microsoft Visual C++ reads and updates this file.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" to indicate parts of the source code you
should add to or customize.

If your application uses MFC in a shared DLL, and your application is 
in a language other than the operating system's current language, you
will need to copy the corresponding localized resources MFC42XXX.DLL
from the Microsoft Visual C++ CD-ROM onto the system or system32 directory,
and rename it to be MFCLOC.DLL.  ("XXX" stands for the language abbreviation.
For example, MFC42DEU.DLL contains resources translated to German.)  If you
don't do this, some of the UI elements of your application will remain in the
language of the operating system.

/////////////////////////////////////////////////////////////////////////////
