#include "stdafx.h"
#include "Receiver.h"

#include "imm.h"
#pragma comment (lib, "imm32.lib")


Receiver::Receiver()
{
	m_screen_step_width = 0;
	m_screen_step_height = 0;

	mp_image = NULL;
	mp_decode_image = NULL;

	mp_mem_dc = NULL;
}


Receiver::Receiver(CWnd *parm_base_wnd)
{
	InitialData(parm_base_wnd);
}


Receiver::~Receiver()
{
	DeleteData();
}


void Receiver::InitialData(CWnd *parm_base_wnd)
{
	CClientDC dc(parm_base_wnd);

	mp_mem_dc = new CDC;
	mp_mem_dc->CreateCompatibleDC(&dc);
	
	int x, y;
	for(y = 0; y < MAX_STEP_COUNT; y++){
		for (x = 0; x < MAX_STEP_COUNT; x++){
			mp_mem_bitmap[y][x] = NULL;
		}
	}

	m_my_screen_width = GetSystemMetrics(SM_CXSCREEN);
	m_my_screen_height = GetSystemMetrics(SM_CYSCREEN);

	// 질문
	m_client_screen_width = m_my_screen_width;
	m_client_screen_height = m_my_screen_height;

}


void Receiver::DeleteData()
{
	int x, y;
	for(y = 0; y < MAX_STEP_COUNT; y++){
		for (x = 0; x < MAX_STEP_COUNT; x++){
			if(mp_mem_bitmap[y][x] != NULL){
				mp_mem_bitmap[y][x]->DeleteObject();
				delete mp_mem_bitmap[y][x];
			}
		}
	}

	if(NULL != mp_image) delete[] mp_image;
	if(NULL != mp_decode_image) delete[] mp_decode_image;	

	if(NULL != mp_mem_dc) {
		mp_mem_dc->DeleteDC();
		delete mp_mem_dc;
	}
}


void Receiver::Decode16to32bits(int parm_width, int parm_height, unsigned char *parm_32_image, unsigned short int *parm_16_image)
{
	int count = parm_width * parm_height;
	unsigned char red, green, blue;
	
	for(int i = 0; i < count; i++){
		red = *parm_16_image >> 11;
		green = *parm_16_image >> 6;
		blue = (unsigned char)*parm_16_image;

		parm_16_image++;

		*parm_32_image++ = red << 3;
		*parm_32_image++ = green << 3;
		*parm_32_image++ = blue << 3;
		parm_32_image++;
	}
}

/*
// RGB(2byte) + 동일 색상 수(1 or 2 byte)
// 16 bit 색상 정보의 6번째 bit 로 동일 색상 수를 저장하는 메모리 크기가 구분되어 있다
// 1 이면 동일한 색상의 수가 2byte 에 저장되어 있다는 의미
// 0 이면 동일한 색상의 수가 1byte 에 저장되어 있다는 의미
void Receiver::Decode16to32bits(unsigned int parm_encode_size, unsigned int *parm_32_image, unsigned char *parm_16_image)
{
	unsigned char red, green, blue;
	int j;
	unsigned short temp_color, short_count;
	unsigned int temp_color_32;
	unsigned int *p_save_pos = parm_32_image;


	for(unsigned int i = 0; i < parm_encode_size; ){
		temp_color = *(unsigned short *)parm_16_image;
		parm_16_image += 2;

		red = temp_color >> 11;
		green = temp_color >> 6;
		blue = (unsigned char)temp_color;

		temp_color_32 = (red << 3) | ((green << 3) << 8)| ((blue << 3) << 16);
		
		// 6 번째 비트 정보 추출
		// 비트 연산에서 비교 대상의 변위에 맞게 비트를 명시해주어야 정확한 연산 결과가 나온다
		// 예를들어 temp_color & 0x20 이렇게 하면 오류가 발생
		if(temp_color & 0x0020){
			short_count = *(unsigned short *)parm_16_image;
			for(j = 0; j < short_count; j++){
				*parm_32_image++ = temp_color_32;
			}
			parm_16_image += 2;
			// 4 byte 만큼 건너뛴다
			i += 4;
		} else {
			for(j = 0; j < *parm_16_image; j++){
				*parm_32_image++ = temp_color_32;
			}
			parm_16_image++;			
			i += 3;
		}
	}
}
*/

//이사님 코드
void Receiver::Decode16to32bits(unsigned int parm_encode_size, unsigned int *parm_p_32_image, unsigned char *parm_p_16_image)
{
	unsigned char temp_r, temp_g, temp_b, count;
	unsigned short temp_color;
	unsigned int temp_color_32, i, j;

	for(i = 0; i < parm_encode_size; i += 3){
		count = *parm_p_16_image++;
		temp_color = *(unsigned short *)parm_p_16_image;
		parm_p_16_image += 2;

		temp_r = temp_color >> 11;
		temp_g = temp_color >> 6;
		temp_b = (unsigned char)temp_color;

		temp_color_32 = (temp_r << 3) | ((temp_g << 3) << 8) | ((temp_b << 3) << 16);

		for(j = 0; j < count; j++){
			*parm_p_32_image++ = temp_color_32;
		}
	}
}



void Receiver::MakeReceiveData(unsigned short int *parm_step_data, CWnd *parm_base_wnd)
{
	int x, y;
	// 다른 해상도의 비트맵이면 갱신

	if(m_screen_step_width != parm_step_data[0] || m_screen_step_height != parm_step_data[1]){
		m_screen_step_width = parm_step_data[0];
		m_screen_step_height = parm_step_data[1];
		
		m_client_screen_width = m_screen_step_width * MAX_STEP_COUNT;
		m_client_screen_height = m_screen_step_height * MAX_STEP_COUNT;
		
		CClientDC dc(parm_base_wnd);

		if(NULL != mp_image) delete[] mp_image;
		if(NULL != mp_decode_image) delete[] mp_decode_image;

		mp_image = new unsigned char[m_screen_step_width * m_screen_step_height * 4];
		mp_decode_image = new unsigned char[m_screen_step_width * m_screen_step_height * 4];
		m_recv_image_size = 0;
		
		for(y = 0; y < MAX_STEP_COUNT; y++){
			for (x = 0; x < MAX_STEP_COUNT; x++){
				if(NULL != mp_mem_bitmap[y][x]){
					mp_mem_bitmap[y][x]->DeleteObject();
					delete mp_mem_bitmap[y][x];									
				}
				mp_mem_bitmap[y][x] = new CBitmap();
				mp_mem_bitmap[y][x]->CreateCompatibleBitmap(&dc, m_screen_step_width, m_screen_step_height);
			}
		}
	}
}


void Receiver::SetNextStepIndex(unsigned short int *parm_body_data)
{
	m_index_x = parm_body_data[0];
	m_index_y = parm_body_data[1];
	m_recv_image_size = 0;
}


unsigned char *Receiver::GetReceiveStepData()
{
	return mp_image + m_recv_image_size;
}


void Receiver::SetCurrentFrameSize(unsigned short parm_size)
{
	m_recv_image_size += parm_size;
}


void Receiver::DecodeStepImage()
{
	if(m_recv_image_size == m_screen_step_width *m_screen_step_height * 2){
		Decode16to32bits(m_screen_step_width, m_screen_step_height, mp_decode_image, (unsigned short int *)mp_image);
	} else Decode16to32bits(m_recv_image_size, (unsigned int *)mp_decode_image, mp_image);
	
	mp_mem_bitmap[m_index_y][m_index_x]->SetBitmapBits(m_screen_step_width *m_screen_step_height * 4, mp_decode_image);
}

void Receiver::InvalidateImage(CDC *parm_dc)
{
	if(mp_mem_bitmap[0][0] != NULL){
		int y, x;
		for(y = 0; y < MAX_STEP_COUNT; y++){
			for (x = 0; x < MAX_STEP_COUNT; x++){
				CBitmap *p_old_bmp = (CBitmap *)mp_mem_dc->SelectObject(mp_mem_bitmap[y][x]);
				
				parm_dc->BitBlt(x * m_screen_step_width, y * m_screen_step_height, m_screen_step_width, m_screen_step_height, mp_mem_dc, 0, 0, SRCCOPY);
				mp_mem_dc->SelectObject(p_old_bmp);
			}
		}
	}
}

void Receiver::ResizeWindow(CWnd *parm_base_wnd)
{
	parm_base_wnd->SetWindowPos(NULL, 0, 0, m_client_screen_width, m_client_screen_height, NULL);

}

// 0617 변경사항 wParam 이 아니라 키 정보를 가진 message로 수정
char Receiver::ReadKeyEvent(MSG *parm_msg, unsigned char *parm_key_data)
{
	char return_val = 0;
	if(parm_msg->message != 21){
		parm_key_data[0] = parm_msg->wParam;
		parm_key_data[1] = 0;
		// 229 한영키의 번호
		if(parm_msg->message == 229) parm_key_data[0] = 21;
		if(GetKeyState(VK_SHIFT) & 0xF000) parm_key_data[1] = 1;

		return_val = 1;

		// 한영키를 눌렀을 경우
		if(0x41f2 == HIWORD(parm_msg->lParam)){
			ChangeEnglishMode(parm_msg->hwnd);
			// 메시지 처리 하지 않게 1 반환
			return_val = 2;
		}
	}
	return return_val;
}
/*
char Receiver::ReadKeyEvent(MSG *parm_msg, unsigned char *parm_key_data)
{
	char return_val = 0;
	if(parm_msg->wParam != 21){
		parm_key_data[0] = parm_msg->wParam;
		parm_key_data[1] = 0;
		// 229 한영키의 번호
		if(parm_msg->wParam == 229) parm_key_data[0] = 21;
		if(GetKeyState(VK_SHIFT) & 0xF000) parm_key_data[1] = 1;

		return_val = 1;

		// 한영키를 눌렀을 경우
		if(0x41f2 == HIWORD(parm_msg->lParam)){
			ChangeEnglishMode(parm_msg->hwnd);
			// 메시지 처리 하지 않게 1 반환
			return_val = 2;
		}
	}
	return return_val;
}

*/

void Receiver::ChangeEnglishMode(HWND parm_handle)
{
	DWORD conv, sentence;

	// Imm 핸들 반환
	HIMC h_imc = ImmGetContext(parm_handle);
	// conversion 모드 정보와 sentence 모드 정보 담기
	ImmGetConversionStatus(h_imc, &conv, &sentence);

	if(IME_CMODE_LANGUAGE * conv){ // 모국어 한국어 모드
		// 입력모드가 한글 상태인 경우에 WM_KEYDOWN 메시지가 제대로 전달되지 못한는 현상이
		// 생겨서 강제로 영문 모드를 유지하도록 수정
		// 모국어 모드 해제
		conv &= (~IME_CMODE_LANGUAGE);
		// 영문 모드 설정
		conv |= IME_CMODE_CHARCODE;
		
		// 변경된 Imm 모드를 세팅
		ImmSetConversionStatus(h_imc, conv, sentence);
	}
	ImmReleaseContext(parm_handle, h_imc);
}


void Receiver::AddStepImage(char *parm_step_image, unsigned short int parm_image_size)
{
	memcpy(mp_image + m_recv_image_size, parm_step_image, parm_image_size);
	m_recv_image_size += parm_image_size;
}