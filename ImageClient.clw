; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=SelectRemoteModeDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "imageclient.h"
LastPage=0

ClassCount=7
Class1=CImageClientApp
Class2=CImageClientDlg
Class3=CImageServerDlg
Class4=InputIpDlg
Class5=RequestChangeModeDlg
Class6=SelectRemoteModeDlg

ResourceCount=7
Resource1=IDD_IMAGESERVER_DIALOG
Resource2=IDD_SELECT_REMOTE_MODE
Resource3=IDD_IMAGECLIENT_DIALOG
Resource4=IDC_REQUEST_CHANGE_MODE
Resource5=IDD_DIALOG
Resource6=IDD_INPUT_IP_DLG
Class7=GuestIpDlg
Resource7=IDD_GUEST_IP_DLG

[CLS:CImageClientApp]
Type=0
BaseClass=CWinApp
HeaderFile=ImageClient.h
ImplementationFile=ImageClient.cpp
Filter=N
LastObject=CImageClientApp

[CLS:CImageClientDlg]
Type=0
BaseClass=MyBiSocket
HeaderFile=ImageClientDlg.h
ImplementationFile=ImageClientDlg.cpp
Filter=D
LastObject=CImageClientDlg
VirtualFilter=uq

[CLS:CImageServerDlg]
Type=0
BaseClass=MyServerSocket
HeaderFile=ImageServerDlg.h
ImplementationFile=ImageServerDlg.cpp
Filter=N
LastObject=IDC_CAPTURE_BTN
VirtualFilter=uq

[CLS:InputIpDlg]
Type=0
BaseClass=CDialog
HeaderFile=InputIpDlg.h
ImplementationFile=InputIpDlg.cpp
Filter=D
LastObject=InputIpDlg

[CLS:RequestChangeModeDlg]
Type=0
BaseClass=CDialog
HeaderFile=RequestChangeModeDlg.h
ImplementationFile=RequestChangeModeDlg.cpp
Filter=D

[CLS:SelectRemoteModeDlg]
Type=0
BaseClass=CDialog
HeaderFile=SelectRemoteModeDlg.h
ImplementationFile=SelectRemoteModeDlg.cpp
Filter=D
LastObject=IDCANCEL

[DLG:IDD_IMAGECLIENT_DIALOG]
Type=1
Class=CImageClientDlg
ControlCount=4
Control1=IDC_EVENT_LIST,listbox,1352728835
Control2=IDC_CAPTURE_BTN,button,1342242816
Control3=IDC_STOP_BTN,button,1342242816
Control4=IDC_REQUIRE_GUEST,button,1342242816

[DLG:IDD_IMAGESERVER_DIALOG]
Type=1
Class=CImageServerDlg
ControlCount=4
Control1=IDC_EVENT_LIST,listbox,1084293379
Control2=IDC_CAPTURE_BTN,button,1073807360
Control3=IDC_STOP_BTN,button,1073807360
Control4=IDC_REQUIRE_CHANGE_MODE,button,1342242816

[DLG:IDD_INPUT_IP_DLG]
Type=1
Class=InputIpDlg
ControlCount=4
Control1=IDC_IP_EDIT,edit,1350631552
Control2=IDOK,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352

[DLG:IDC_REQUEST_CHANGE_MODE]
Type=1
Class=RequestChangeModeDlg
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352

[DLG:IDD_SELECT_REMOTE_MODE]
Type=1
Class=SelectRemoteModeDlg
ControlCount=14
Control1=IDC_GUEST_BTN,button,1342242816
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDC_HOST_BTN,button,1342242816
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,button,1342177287
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352
Control12=IDC_STATIC,static,1342308352
Control13=IDC_STATIC,static,1342308352
Control14=IDC_STATIC,button,1342177287

[DLG:IDD_DIALOG]
Type=1
Class=?
ControlCount=0

[DLG:IDD_GUEST_IP_DLG]
Type=1
Class=GuestIpDlg
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDC_IP_LIST,listbox,1352728835
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352

[CLS:GuestIpDlg]
Type=0
HeaderFile=GuestIpDlg.h
ImplementationFile=GuestIpDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_IP_LIST

