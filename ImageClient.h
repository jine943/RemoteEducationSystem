// ImageClient.h : main header file for the IMAGECLIENT application
//

#if !defined(AFX_IMAGECLIENT_H__1586107B_6438_4FE5_8889_1C6611747072__INCLUDED_)
#define AFX_IMAGECLIENT_H__1586107B_6438_4FE5_8889_1C6611747072__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CImageClientApp:
// See ImageClient.cpp for the implementation of this class
//

class CImageClientApp : public CWinApp
{
public:
	CImageClientApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImageClientApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CImageClientApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMAGECLIENT_H__1586107B_6438_4FE5_8889_1C6611747072__INCLUDED_)
