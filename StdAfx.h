// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__EB58C67F_CB54_4D13_9944_9D8E76CDB826__INCLUDED_)
#define AFX_STDAFX_H__EB58C67F_CB54_4D13_9944_9D8E76CDB826__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <imm.h>
#include <WinSock2.h>
#pragma comment (lib, "WS2_32.lib")

#define MAX_PACKET_SIZE		4096
#define MAX_STEP_COUNT      10
#define MAX_IP_LEN          24
#define MAX_IP_COUNT		10
#define HOST_MODE			0
#define GUEST_MODE			1
#define DESIRED_WINSOCK_VERSION		0x0202
#define MINIMUM_WINSOCK_VERSION		0x0001


#define LM_CONNECT_MESSAGE		     27001
#define LM_ACCEPT_MESSAGE            27002
#define LM_REQUIRE_CHANGE_MODE       27003


#define NM_SEND_STEP_SIZE       1 // C -> S
#define NM_REQUEST_STEP_INDEX   2 // C <- S
#define NM_NEXT_STEP_INDEX      3 // C -> S
#define NM_REQUEST_STEP_DATA    4 // C <- S
#define NM_SEND_STEP_DATA       5 // C -> S
#define NM_SEND_LAST_STEP_DATA  6 // C -> S
#define NM_INVALIDATE_IMAGE     7 // C -> S
#define NM_REQUIRE_CHANGE_MODE  8 // C <-> S
#define NM_RESULT_CHANGE_MODE   9 // C <-> S

extern char g_mode;
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__EB58C67F_CB54_4D13_9944_9D8E76CDB826__INCLUDED_)
