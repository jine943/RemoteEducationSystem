#include "stdafx.h"
#include "MyServerSocket.h"

MyServerSocket::MyServerSocket(int parm_dlg_id, CWnd* pParent) : MySocket(parm_dlg_id, pParent)
{
	mh_listen_socket = INVALID_SOCKET;
}


BEGIN_MESSAGE_MAP(MyServerSocket, MySocket)
 	ON_MESSAGE(LM_ACCEPT_MESSAGE, OnAcceptSockProcess)
END_MESSAGE_MAP()


void MyServerSocket::ConnectClient(char *parm_ip, int parm_port)
{
	mh_listen_socket = socket(AF_INET, SOCK_STREAM, 0);

	struct sockaddr_in srv_addr;
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = inet_addr(parm_ip);
	srv_addr.sin_port = htons(parm_port);

	bind(mh_listen_socket, (LPSOCKADDR)&srv_addr, sizeof(struct sockaddr_in));
	listen(mh_listen_socket, 1);

	WSAAsyncSelect(mh_listen_socket, this->m_hWnd, LM_ACCEPT_MESSAGE, FD_ACCEPT);
}


LRESULT MyServerSocket::OnAcceptSockProcess(WPARAM wParam, LPARAM lParam)
{
	if(INVALID_SOCKET == mh_socket){
		sockaddr_in client_addr;
		int client_addr_size = sizeof(sockaddr_in);
		
		mh_socket = accept((SOCKET)wParam, (LPSOCKADDR)&client_addr, &client_addr_size);
		m_client_ip_address = inet_ntoa(client_addr.sin_addr);

		WSAAsyncSelect(mh_socket, m_hWnd, LM_DATA_SOCKET_MESSAGE, FD_READ | FD_CLOSE);

		AddEventList("새로운 클라이언트가 접속했습니다 -> " + m_client_ip_address);
	}
	return 1;
}

