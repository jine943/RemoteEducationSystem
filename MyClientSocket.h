#ifndef _MY_CLIENT_SOCKET_H_
#define _MY_CLIENT_SOCKET_H_

#include "MySocket.h"

class MyClientSocket : public MySocket
{
public:
	MyClientSocket(int parm_dlg_id, CWnd* pParent = NULL);

	void ConnectServer(char *parm_ip, int parm_port);

	LRESULT OnConnectMessage(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

#endif
