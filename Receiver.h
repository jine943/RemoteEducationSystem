#ifndef _RECEIVER_H_
#define _RECEIVER_H_

class Receiver
{
private:
	CDC *mp_mem_dc;
	CBitmap *mp_mem_bitmap[MAX_STEP_COUNT][MAX_STEP_COUNT];

	int m_my_screen_width, m_my_screen_height;
	int m_client_screen_width, m_client_screen_height;
	int m_screen_step_width, m_screen_step_height;

	unsigned short int m_index_x, m_index_y;
	// 받은 이미지 
	unsigned char *mp_image, *mp_decode_image;
	int m_recv_image_size;

public:
	Receiver();
	Receiver(CWnd *parm_base_wnd);
	~Receiver();

	void DeleteData();

	void InitialData(CWnd *parm_base_wnd);

	void MakeReceiveData(unsigned short int *parm_step_data, CWnd *parm_base_wnd);

	void SetNextStepIndex(unsigned short int *parm_body_data);

	unsigned char *GetReceiveStepData();
	void SetCurrentFrameSize(unsigned short parm_size);
	void AddStepImage(char *parm_step_image, unsigned short int parm_image_size);
	
	void DecodeStepImage();
	void Decode16to32bits(int parm_width, int parm_height, unsigned char *parm_32_image, unsigned short int *parm_16_image);
	void Decode16to32bits(unsigned int parm_encode_size, unsigned int *parm_32_image, unsigned char *parm_16_image);

	//void Decode16to32bits(unsigned int parm_encode_size, unsigned char *parm_p_16_image, unsigned int *parm_p_32_image);

	void InvalidateImage(CDC *parm_dc);

	void ResizeWindow(CWnd *parm_base_wnd);

	char ReadKeyEvent(MSG *parm_msg, unsigned char *parm_key_data);

	void ChangeEnglishMode(HWND parm_handle);
};

#endif
