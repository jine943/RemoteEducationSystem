#if !defined(AFX_GUESTIPDLG_H__0FF3A17D_8A32_491A_A4B5_F625A7BE4012__INCLUDED_)
#define AFX_GUESTIPDLG_H__0FF3A17D_8A32_491A_A4B5_F625A7BE4012__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GuestIpDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// GuestIpDlg dialog

class GuestIpDlg : public CDialog
{
private:
	char m_ip[MAX_IP_COUNT][MAX_IP_LEN];
	char m_selected_ip[MAX_IP_LEN];

public:
	GuestIpDlg(CWnd* pParent = NULL);   // standard constructor
	char *GetServerIp();

// Dialog Data
	//{{AFX_DATA(GuestIpDlg)
	enum { IDD = IDD_GUEST_IP_DLG };
	CListBox	m_ip_list;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GuestIpDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GuestIpDlg)
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GUESTIPDLG_H__0FF3A17D_8A32_491A_A4B5_F625A7BE4012__INCLUDED_)
