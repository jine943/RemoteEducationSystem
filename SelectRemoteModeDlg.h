#if !defined(AFX_SELECTREMOTEMODEDLG_H__2D8D3BA6_04FC_4B99_B878_68F20521D906__INCLUDED_)
#define AFX_SELECTREMOTEMODEDLG_H__2D8D3BA6_04FC_4B99_B878_68F20521D906__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectRemoteModeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SelectRemoteModeDlg dialog

class SelectRemoteModeDlg : public CDialog
{
private:
	//0 : Host 1: Guest
	char m_mode;
// Construction
public:
	SelectRemoteModeDlg(CWnd* pParent = NULL);   // standard constructor
	char GetRemoteMode();

// Dialog Data
	//{{AFX_DATA(SelectRemoteModeDlg)
	enum { IDD = IDD_SELECT_REMOTE_MODE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SelectRemoteModeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(SelectRemoteModeDlg)
	afx_msg void OnHostBtn();
	afx_msg void OnGuestBtn();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTREMOTEMODEDLG_H__2D8D3BA6_04FC_4B99_B878_68F20521D906__INCLUDED_)
