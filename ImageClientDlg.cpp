// ImageClientDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ImageClient.h"
#include "ImageClientDlg.h"
#include "RequestChangeModeDlg.h"


#include "imm.h"
#pragma comment (lib, "imm32.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImageClientDlg dialog

CImageClientDlg::CImageClientDlg(CWnd* pParent): MyBiSocket(CImageClientDlg::IDD, pParent)
{	
	//{{AFX_DATA_INIT(CImageClientDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_start_flag = 0;
}

void CImageClientDlg::DoDataExchange(CDataExchange* pDX)
{
	MyBiSocket::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CImageClientDlg)
	DDX_Control(pDX, IDC_EVENT_LIST, m_event_list);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CImageClientDlg, MyBiSocket)
	//{{AFX_MSG_MAP(CImageClientDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CAPTURE_BTN, OnCaptureBtn)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_STOP_BTN, OnStopBtn)
	ON_BN_CLICKED(IDC_REQUIRE_GUEST, OnRequireGuest)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_MOUSEWHEEL()
	//}}AFX_MSG_MAP
	ON_MESSAGE(LM_REQUIRE_CHANGE_MODE, ReqireChangeMode)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImageClientDlg message handlers

LRESULT CImageClientDlg::ReqireChangeMode(WPARAM wParam, LPARAM lParam)
{
	char flag  = 0;

	if(0 == lParam){
		RequestChangeModeDlg ins_dlg;
		if(IDOK == ins_dlg.DoModal()) flag = 1;
	}	
	if(flag || lParam){
		if(GUEST_MODE == g_mode) {
			g_mode = HOST_MODE;
		} else {
			g_mode = GUEST_MODE;
			//그림 초기화 해줘~! 다시 게스트 될 때
		}
		
		ChangeDisplayProperty(g_mode);

		if(lParam == 0) SendFrameData(mh_socket, NM_RESULT_CHANGE_MODE, NULL, 0);
	}
	return 1;		
}


BOOL CImageClientDlg::OnInitDialog()
{
	MyBiSocket::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	mp_event_list = &m_event_list;

	m_receiver.InitialData(this);
	m_sender.InitialData(this);

	Connect(26000);
	//58.233.158.53
	
	ChangeDisplayProperty(g_mode);
	//if(GUEST_MODE == g_mode) m_receiver.ResizeWindow(this);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CImageClientDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	if (IsIconic())
	{
		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		if(GUEST_MODE == g_mode) m_receiver.InvalidateImage(&dc);
		else MyBiSocket::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CImageClientDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CImageClientDlg::ReadSocketData(SOCKET parm_socket, unsigned short parm_size, unsigned char parm_index, char *parm_body_data)
{
	unsigned short int *p_next;
	if(GUEST_MODE == g_mode){
		if(NM_SEND_STEP_SIZE == parm_index){
			
			m_receiver.MakeReceiveData((unsigned short int *)parm_body_data, this);
			SendFrameData(parm_socket, NM_REQUEST_STEP_INDEX, NULL, 0);
			
			// 날아오는 그림의 행열을 받는다
		} else if(NM_NEXT_STEP_INDEX == parm_index){
			
			m_receiver.SetNextStepIndex((unsigned short int *)parm_body_data);
			SendFrameData(parm_socket, NM_REQUEST_STEP_DATA, NULL, 0);
			
			// 100 분할 이미지에서 중간 1KB
		} else if(NM_SEND_STEP_DATA == parm_index){
			
			m_receiver.AddStepImage(parm_body_data, parm_size);
			//memcpy(m_receiver.GetReceiveStepData(), parm_body_data, parm_size);
			//m_receiver.SetCurrentFrameSize(parm_size);
			
			// 4번 메시지. 1KB 받았다 더 보내
			SendFrameData(parm_socket, NM_REQUEST_STEP_DATA, NULL, 0);
			
			// 100 분할 이미지에서 마지막 1KB
		} else if(NM_SEND_LAST_STEP_DATA == parm_index){
			
			m_receiver.AddStepImage(parm_body_data, parm_size);
			//memcpy(m_receiver.GetReceiveStepData(), parm_body_data, parm_size);
			//m_receiver.SetCurrentFrameSize(parm_size);
			
			m_receiver.DecodeStepImage();
			
			SendFrameData(parm_socket, NM_REQUEST_STEP_INDEX, NULL, 0);
			
		} else if(NM_INVALIDATE_IMAGE == parm_index){
			Invalidate();
		} 
	// HOST_MODE
	} else {
		if(NM_REQUEST_STEP_INDEX == parm_index){
			p_next = m_sender.NextStepIndex();
			
			if(NULL == p_next){
				SendFrameData(parm_socket, NM_INVALIDATE_IMAGE, NULL, 0);
				
				if(m_start_flag == 1) SetTimer(1, 50, NULL);
			} else {
				m_sender.CalTotalSize();
				SendFrameData(parm_socket, NM_NEXT_STEP_INDEX, p_next, sizeof(unsigned short int) * 2);
			}
			
		} else if(NM_REQUEST_STEP_DATA == parm_index){
			
			if(m_sender.IsExistNextStep() == 0){
				SendFrameData(parm_socket, NM_SEND_LAST_STEP_DATA, m_sender.GetSendStepData(), m_sender.GetCurrentFrameSize());
			} else SendFrameData(parm_socket, NM_SEND_STEP_DATA, m_sender.GetSendStepData(), m_sender.GetCurrentFrameSize());
			
			//SendFrameData(parm_socket, 5 + index, m_sender.GetSendStepData(), m_sender.GetCurrentFrameSize());
			
			m_sender.NextFrame();
			
		} else if(20 == parm_index){
			// 마우스 위치를 읽어들인다
			m_sender.WriteMouseEvent((int *)parm_body_data, MOUSEEVENTF_MOVE);
			
		} else if(21 == parm_index){
			m_sender.WriteMouseEvent((int *)parm_body_data, MOUSEEVENTF_LEFTDOWN);
			
		} else if(22 == parm_index){
			m_sender.WriteMouseEvent((int *)parm_body_data, MOUSEEVENTF_LEFTUP);
			
		} else if(30 == parm_index){
			m_sender.WriteKeyEvent((unsigned char *)parm_body_data);

		} else if(24 == parm_index){
			BYTE key_code = *(BYTE *)parm_body_data;
			keybd_event(key_code, 0, KEYEVENTF_KEYUP, 0);
		} else if(23 == parm_index){
			m_sender.WriteMouseEvent((int *)parm_body_data, MOUSEEVENTF_LEFTUP);
		} else if(25 == parm_index){
			m_sender.WriteMouseEvent((int *)parm_body_data, MOUSEEVENTF_LEFTDOWN);
			m_sender.WriteMouseEvent((int *)parm_body_data, MOUSEEVENTF_LEFTUP);
			Sleep(30);
			m_sender.WriteMouseEvent((int *)parm_body_data, MOUSEEVENTF_LEFTDOWN);
			m_sender.WriteMouseEvent((int *)parm_body_data, MOUSEEVENTF_LEFTUP);
		} else if(26 == parm_index){
			m_sender.WriteMouseEvent((int *)parm_body_data, MOUSEEVENTF_WHEEL);
		}
	} 
	
	if(NM_REQUIRE_CHANGE_MODE == parm_index){
		PostMessage(LM_REQUIRE_CHANGE_MODE, 0, 0);
	} else if(NM_RESULT_CHANGE_MODE == parm_index){
		PostMessage(LM_REQUIRE_CHANGE_MODE, 0, 1);
	}
}


void CImageClientDlg::ChangeDisplayProperty(char parm_mode)
{
	CString str;
	if(HOST_MODE == parm_mode)
	{
		GetDlgItem(IDC_CAPTURE_BTN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STOP_BTN)->ShowWindow(SW_SHOW);
		// GetDlgItem(IDOK)->ShowWindow(SW_SHOW);
		// GetDlgItem(IDCANCEL)->ShowWindow(SW_SHOW);
		m_event_list.ShowWindow(SW_SHOW);
		
		SetWindowPos(NULL, 0, 0, 370, 160, NULL);
		//str.Format("Host 모드입니다.");
		//SetWindowText(str);
		ModifyStyle(NULL, WS_CAPTION | WS_DLGFRAME | WS_SYSMENU | WS_BORDER);
		Invalidate();
		
	} else {		
		GetDlgItem(IDC_CAPTURE_BTN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STOP_BTN)->ShowWindow(SW_HIDE);
		// GetDlgItem(IDOK)->ShowWindow(SW_HIDE);
		// GetDlgItem(IDCANCEL)->ShowWindow(SW_HIDE);
		m_event_list.ShowWindow(SW_HIDE);
		
		//str.Format("Guest 모드입니다.");
		//SetWindowText(str);
		SetWindowPos(NULL, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), NULL);
		
		ModifyStyle(WS_CAPTION | WS_DLGFRAME | WS_SYSMENU | WS_BORDER, NULL);
	}
}

void CImageClientDlg::OnDestroy() 
{
	MyBiSocket::OnDestroy();
}

// 메시지 전송 첫 번째 단계
void CImageClientDlg::OnCaptureBtn() 
{
	if(m_start_flag == 0){
		SetTimer(1, 1000, NULL);	
		m_start_flag = 1;
	}
}

void CImageClientDlg::OnTimer(UINT nIDEvent) 
{
	if(1 == nIDEvent){	
		KillTimer(1);
		if(mh_socket != INVALID_SOCKET){
			AddEventList("화면을 캡쳐합니다.");
			
			m_sender.MakeSendData();
			SendFrameData(mh_socket, NM_SEND_STEP_SIZE, m_sender.GetStepSize(), sizeof(unsigned short int) * 2);
		} else AddEventList("서버와 접속된 상태가 아닙니다.");
	} else MyBiSocket::OnTimer(nIDEvent);
}	

void CImageClientDlg::OnStopBtn() 
{
	KillTimer(1);
	m_start_flag = 0;
}

void CImageClientDlg::OnRequireGuest() 
{
	SendFrameData(mh_socket, NM_REQUIRE_CHANGE_MODE, NULL, 0);
	OnStopBtn();
}

void CImageClientDlg::SetIP(const char *parm_ip)
{
	memcpy(m_ip, parm_ip, MAX_IP_LEN);
}

void CImageClientDlg::OnMouseMove(UINT nFlags, CPoint point) 
{
	if(INVALID_SOCKET != mh_socket && GUEST_MODE == g_mode){
		SendFrameData(mh_socket, 20, &point, sizeof(point));
	}

	MyBiSocket::OnMouseMove(nFlags, point);
}

void CImageClientDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if(INVALID_SOCKET != mh_socket && GUEST_MODE == g_mode){
		SendFrameData(mh_socket, 21, &point, sizeof(point));
	}
	MyBiSocket::OnLButtonDown(nFlags, point);
}

void CImageClientDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if(INVALID_SOCKET != mh_socket && GUEST_MODE == g_mode){
		SendFrameData(mh_socket, 22, &point, sizeof(point));
	}
	
	MyBiSocket::OnLButtonUp(nFlags, point);
}


void CImageClientDlg::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	if(INVALID_SOCKET != mh_socket && GUEST_MODE == g_mode){
		SendFrameData(mh_socket, 25, &point, sizeof(point));
	}
	
	MyBiSocket::OnLButtonDblClk(nFlags, point);
}

// 컨트롤 키 누르고 제어권 전환 버튼 옮길 수 있게 만들기

BOOL CImageClientDlg::PreTranslateMessage(MSG* pMsg) 
{
	if(GUEST_MODE == g_mode){
		if(WM_KEYDOWN == pMsg->message){
			unsigned char keyboard_data[2];
			char ret_val = m_receiver.ReadKeyEvent(pMsg, keyboard_data);
			if(ret_val){
				SendFrameData(mh_socket, 30, keyboard_data, sizeof(keyboard_data));
				if(2 == ret_val) return 1; 
			}
		}
	}
	// 메시지 돌려주기 
	return MyBiSocket::PreTranslateMessage(pMsg);
}

BOOL CImageClientDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
/*	int data[3];

	data[0] = pt.x;
	data[1] = pt.y;
	data[2] = (int)zDelta;
*/
	char data[10];
	*(int *)data = pt.x;
	*((int *)data + 1) = pt.y;
	*((short *)data + 4) = zDelta;

	if(INVALID_SOCKET != mh_socket && GUEST_MODE == g_mode){
		SendFrameData(mh_socket, 26, &data, sizeof(data));
	}
	
	return MyBiSocket::OnMouseWheel(nFlags, zDelta, pt);
}

